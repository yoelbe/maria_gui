import psutil
import pynvml
import gc
from config import *

class MemoryManager:
    """Manage the memory mode and release it when needed."""

    def free_memory(self, tokenizer, model, model_name):
        """
        Release the memory according to the behavior of memory mode settings.

        * Minimal: Always release cache after loading and use a model to economize memory.
        * Auto: Clear the current model cache if it exceeds the threshold of 80% of system memory.
        * Manual: Cache only the selected models, but still follows the auto behavior if there is not enough memory.

        :param tokenizer: The tokenizer object.
        :type tokenizer: transformers.PreTrainedTokenizer
        :param model: The model object.
        :type model: transformers.PreTrainedModel
        :param model_name: The name of the model.
        :type model_name: str
    """
        
        global memory_mode
        
        threshold_exceeded = self.get_ram_usage() > 80 or self.get_gpu_memory_usage() > 80
        cache_enabled = available_models[model_name]['cache']

        if (
            memory_mode == "minimal" or
            (memory_mode == "auto" and threshold_exceeded) or
            (memory_mode == "manual" and (not cache_enabled or threshold_exceeded))
        ):
            del tokenizer, model
            gc.collect()
            
    
    def change_memory_mode(self, mode):
        """
        Set the chosen memory mode.

        :param mode: The desired memory mode.
        :type mode: str
        """

        global memory_mode
        memory_mode = mode

    def get_ram_usage(self):
        """
        Returns the RAM usage in percentage.

        :return: The RAM usage percentage.
        :rtype: float
        """

        ram = psutil.virtual_memory()
        ram_usage = ram.percent
        return ram_usage

    def get_gpu_memory_usage(self):
        """
        Returns the GPU memory usage in percentage.

        :return: The GPU memory usage percentage.
        :rtype: float
        """

        pynvml.nvmlInit()
        # Get GPU Id, 0 for first GPU
        handle = pynvml.nvmlDeviceGetHandleByIndex(0)  

        memory_info = pynvml.nvmlDeviceGetMemoryInfo(handle)

        memory_usage = (memory_info.used / memory_info.total ) * 100
        memory_usage = round(memory_usage, 1) 

        pynvml.nvmlShutdown()

        return memory_usage