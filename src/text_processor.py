from transformers import pipeline
import torch

class TextProcessor:   
    """Use multiple models for Natural Language Processing.""" 

    def process_text(self, tokenizer, model, text_list, task, inputs):
        """
        Perform the task to process the input text with the given model and tokenizer and returns the output.
        
        :param tokenizer: The tokenizer object.
        :type tokenizer: transformers.PreTrainedTokenizer
        :param model: The model object.
        :type model: transformers.PreTrainedModel
        :param text_list: The list of input texts.
        :type text_list: list
        :param task: The task to perform.
        :type task: str
        :param inputs: The type of input ('one', 'two', or 'concatenated').
        :type inputs: str
        :return: The output result of the text processing.
        :rtype: str
        """

        result = ""

        # Enable GPU CUDA Acceleration
        device_index = 0
        if torch.cuda.is_available():
            device = torch.device(f"cuda:{device_index}")
        else:
            device = torch.device("cpu")

        # Process the text based on the pipeline input behaviour
        if inputs == "one":
            nlp = pipeline(task, model=model, tokenizer=tokenizer, device=device)
            result = nlp(text_list[0])
            
        elif inputs == "two":
            nlp = pipeline(task, model=model, tokenizer=tokenizer, device=device)
            result = nlp(text_list[0], text_list[1])

        elif inputs == "concatenated":
            nlp = pipeline(task, model=model, tokenizer=tokenizer, device=device)
            result = nlp(text_list[0] + " " + text_list[1])   

        return result