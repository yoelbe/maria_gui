from transformers import AutoModelForQuestionAnswering, AutoModelForMaskedLM, AutoModelForSequenceClassification, AutoModelForTokenClassification

"""Global settings"""

MODELS_PATH = "../models/"

memory_mode = "auto" # minimal | auto | manual

available_models = {
    'Fill mask': {
        'cache': False,
        'repository': 'PlanTL-GOB-ES/roberta-large-bne',
        'model_class': AutoModelForMaskedLM,
        'task': 'fill-mask',
        'inputs': 'one'
    },
    'Question Answering': {
        'cache': False,
        'repository': 'PlanTL-GOB-ES/roberta-large-bne-sqac',
        'model_class': AutoModelForQuestionAnswering,
        'task': 'question-answering',
        'inputs': 'two'
    },
    'Controversy prediction': {
        'cache': False,
        'repository': 'PlanTL-GOB-ES/Controversy-Prediction',
        'model_class': AutoModelForSequenceClassification,
        'task': 'text-classification',
        'inputs': 'one'
    },
    'Text entailment': {
        'cache': False,
        'repository': 'PlanTL-GOB-ES/roberta-large-bne-te',
        'model_class': AutoModelForSequenceClassification,
        'task': 'text-classification',
        'inputs': 'concatenated'
    },
    'Part of speech': {
        'cache': False,
        'repository': 'PlanTL-GOB-ES/roberta-large-bne-capitel-pos',
        'model_class': AutoModelForTokenClassification,
        'task': 'token-classification',
        'inputs': 'one'
    },
    'Named entity recognition': {
        'cache': False,
        'repository': 'PlanTL-GOB-ES/roberta-large-bne-capitel-ner',
        'model_class': AutoModelForTokenClassification,
        'task': 'ner',
        'inputs': 'one'
    },
    'Classify': {
        'cache': False,
        'repository': 'PlanTL-GOB-ES/roberta-base-es-wikicat-es',
        'model_class': AutoModelForSequenceClassification,
        'task': 'text-classification',
        'inputs': 'one'
    }
}