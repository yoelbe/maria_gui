from gui_manager import GUIManager

def main():
    """ Instance the user interface and runs it.
        Advanced users who want to explore the code deeper can easily access functionality through the Model Manager Facade""" 
    
    gui_manager = GUIManager() 
    gui_manager.run()
    
if __name__ == "__main__":
    main()