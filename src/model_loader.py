from transformers import AutoTokenizer
import os
from config import *

class ModelLoader:
    """Retrive the model from a local source and load into memory."""
    
    def load_model(self, model_name):
        """
        Loads the model and the tokenizer locally and returns both variables.

        :param model_name: The name of the model.
        :type model_name: str
        :return: The tokenizer and model loaded from the local source.
        :rtype: tuple
        """

        model_path = os.path.join(MODELS_PATH, model_name)
        model_class = available_models[model_name]['model_class']

        tokenizer = AutoTokenizer.from_pretrained(model_path)
        model = model_class.from_pretrained(model_path)

        return tokenizer, model