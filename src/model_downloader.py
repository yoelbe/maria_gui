from transformers import AutoTokenizer
import os
from config import *

class ModelDownloader:
    """Download online models"""

    def download_model(self, model_name):
        """
        Downloads the model and tokenizer from the online repository and save them locally.

        :param model_name: The name of the model.
        :type model_name: str
        """

        model_path = os.path.join(MODELS_PATH, model_name)        
        model_class = available_models[model_name]['model_class']
        repository = available_models[model_name]['repository']

        tokenizer = AutoTokenizer.from_pretrained(repository)
        model = model_class.from_pretrained(repository)

        tokenizer.save_pretrained(model_path)
        model.save_pretrained(model_path)

