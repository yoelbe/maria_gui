from model_downloader import ModelDownloader
from model_loader import ModelLoader
from memory_manager import MemoryManager
from text_processor import TextProcessor
import os
from config import *

class ModelManagerFacade:
    """The logic core of the facade pattern design, it provides a simple interface to delegate multiple model requests and their lifecycle. """

    def __init__(self):
        """Create the directory structure for storing the models and instance the specific classes"""

        self.create_directories()

        self.model_downloader = ModelDownloader()
        self.model_loader = ModelLoader()
        self.memory_manager = MemoryManager()
        self.text_processor = TextProcessor()
        

    def create_directories(self):
        """Create the directory structure for storing the models."""

        def create_folder(folder_path):
            """Handler function to check if a directory exists before creating it."""

            if not os.path.exists(folder_path):
                os.makedirs(folder_path)

        create_folder(MODELS_PATH)

        for model in available_models:
            model_directory = os.path.join(MODELS_PATH, model)
            create_folder(model_directory)
            
            

    def process_text(self, model_name, text_list, task):
        """
        Logic to perform the lifecycle of a text processing:
        
        1. Receives the model name to use and a text list with 2 elements:
        * 1: input, mandatory 
        * 2: context, used only for certain models
        
        2. Load the model locally, if it doesn't exists it will download it from the online repository first.

        3. Process the text with the chosen model

        4. Returns the result
        
        :param model_name: The name of the model to use.
        :type model_name: str
        :param text_list: The list of input texts.
        :type text_list: list
        :param task: The task to perform.
        :type task: str
        :param inputs: The type of input ('one', 'two', or 'concatenated').
        :type inputs: str
        :return: The output result of the text processing.
        :rtype: str
        """

        try:
            tokenizer, model = self.model_loader.load_model(model_name)
        except:
            self.model_downloader.download_model(model_name)
            tokenizer, model = self.model_loader.load_model(model_name)

        inputs = available_models[model_name]['inputs']

        result = self.text_processor.process_text(tokenizer, model, text_list, task, inputs)

        self.memory_manager.free_memory(tokenizer, model, model_name)

        return result