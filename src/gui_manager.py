import tkinter as tk
from tkinter import ttk
from model_manager_facade import ModelManagerFacade
from config import *

class GUIManager:
    """ 
    The user interface provides an intuitive way for the user to interact with the AI models.
    It also allows you to configure memory settings, as well as monitor RAM and GPU memory usage.
    """
    
    def __init__(self):
        """
        Generates a tabbed window for managing models and memory.

        * Models tab:
        1: Choose a model from the dropdown, enter the input, and just in case, a context as well.
        2: Click the Process button
        3. Check the result in the output box

        * Memory tab:
        1: Select the memory configuration that best suits your needs.
        * Minimal, for machines with low memory resources, however, it needs more computing time due to reloads of various models
        * Automatic, useful for testing multiple models multiple times, each model is used while memory usage is 80% triple
        * Manual, best for advanced users who want to cache selected models as long as memory is available for recurring use
        2: model box list
        * Select the models you want to cache when using manual mode
        3: memory monitor
        * A bar graph to check RAM/GPU usage whenever models are cached and memory freed. 
        """
        
        # Instance Managers
        self.model_manager = ModelManagerFacade()

        # Create the Window
        self.window = tk.Tk()
        self.window.title("MarIA GUI")
        self.window.geometry("800x500")

        # Create a Notebook object for the tabs
        self.notebook = ttk.Notebook(self.window)

        # MODELS TAB
        self.models_tab = ttk.Frame(self.notebook)
        self.notebook.add(self.models_tab, text="MODELS")

        # Label for dropdown model selecor
        self.model_type_label = tk.Label(self.models_tab, text="Model type")
        self.model_type_label.pack()

        # Dropdown model selector
        self.model_selection = ttk.Combobox(
            self.models_tab,
            values=list(available_models),
            state="readonly"
        )
        self.model_selection.pack()

        # Label for input text box
        self.input_label = tk.Label(self.models_tab, text="Input")
        self.input_label.pack()

        # Input text box
        self.input_text = tk.Text(self.models_tab, height=5)
        self.input_text.pack()

        # Label for result text box
        self.result_label = tk.Label(self.models_tab, text="Result")
        self.result_label.pack()

        # Crear el cuadro de texto dentro del marco
        self.result_text = tk.Text(self.models_tab, height=8, state="disabled")
        self.result_text.pack()

        # Process button
        self.process_button = tk.Button(self.models_tab, text="Process", command=self.process_text)
        self.process_button.pack()

        # Blank separation
        separator_frame = tk.Frame(self.models_tab, height=10)
        separator_frame.pack()

        # Label for context text box
        self.context_label = tk.Label(self.models_tab, text="Context input (only for question answering and text entailment)")
        self.context_label.pack()

        # Content text box
        self.context_text = tk.Text(self.models_tab, height=5)
        self.context_text.pack()

        # MEMORY TAB
        self.memory_tab = ttk.Frame(self.notebook)
        self.notebook.add(self.memory_tab, text="MEMORY")

        # Add content to the second tab
        self.mode_label = tk.Label(self.memory_tab, text="Mode")
        self.mode_label.pack()

        # Create the mode radio buttons 
        radio_memory_mode = tk.StringVar()

        radio_minimal_mode = tk.Radiobutton(
            self.memory_tab,
            text="Minimal: Clear memory after each process. Recommended for machines with few resources",
            variable=radio_memory_mode,
            value="minimal",
            command=lambda: self.model_manager.memory_manager.change_memory_mode(radio_memory_mode.get())
        )
        radio_minimal_mode.pack(anchor="w")

        radio_auto_mode = tk.Radiobutton(
            self.memory_tab,
            text="Auto: Cache all and clear memory only when no more is available. Recomeded for testing multiple models",
            variable=radio_memory_mode,
            value="auto",
            command=lambda: self.model_manager.memory_manager.change_memory_mode(radio_memory_mode.get())
        )
        radio_auto_mode.pack(anchor="w")

        radio_manual_mode = tk.Radiobutton(
            self.memory_tab,
            text="Manual: Cache the selected models as long as memory is available for optimal loading times. Recomended for advanced users",
            variable=radio_memory_mode,
            value="manual",
            command=lambda: self.model_manager.memory_manager.change_memory_mode(radio_memory_mode.get())
        )
        radio_manual_mode.pack(anchor="w")

        # Set the default selected radio button
        radio_memory_mode.set("auto")

        # Blank separation
        separator_frame = tk.Frame(self.memory_tab, height=10)
        separator_frame.pack()

         # Label for checkboxes
        self.checkboxes_label = tk.Label(self.memory_tab, text="Select models to cache")
        self.checkboxes_label.pack(anchor="w")

        # Create a list to store the checkboxes
        self.checkbox_list = []

        # Create checkboxes for the "text" models
        for model, cache_var in available_models.items():
            checkbox_var = tk.IntVar(value=cache_var)
            checkbox = tk.Checkbutton(
                self.memory_tab,
                text=model,
                variable=checkbox_var,
                command=lambda model=model, var=checkbox_var: self.toggle_model_cache(model, var.get())
            )
            checkbox.pack(anchor="w")
            self.checkbox_list.append((model, checkbox_var))

        # Label for memory statistics
        self.memory_statistics_label = tk.Label(self.memory_tab, text="Memory statistics")
        self.memory_statistics_label.pack()

        # Blank separation
        separator_frame = tk.Frame(self.memory_tab, height=10)
        separator_frame.pack()

        # Create the RAM progress bar
        self.ram_progress_bar = ttk.Progressbar(self.memory_tab, length=200, mode='determinate')
        self.ram_progress_bar.pack()

        # Label for RAM bar
        self.ram_usage_label = tk.Label(self.memory_tab, text="")
        self.ram_usage_label.pack()

        # Blank separation
        separator_frame = tk.Frame(self.memory_tab, height=10)
        separator_frame.pack()

        # Create the GPU progress bar
        self.gpu_progress_bar = ttk.Progressbar(self.memory_tab, length=200, mode='determinate')
        self.gpu_progress_bar.pack()

        # Label for GPU bar
        self.gpu_usage_label = tk.Label(self.memory_tab, text="")
        self.gpu_usage_label.pack()

        # Display the tabs in the main window
        self.notebook.pack(expand=True, fill="both")
            
    def update_ram_usage(self):
        ram_usage = self.model_manager.memory_manager.get_ram_usage()
        self.ram_progress_bar['value'] = ram_usage
        self.ram_usage_label.config(text=f"RAM: {ram_usage}%")
        self.window.after(1000, self.update_ram_usage)

    def update_gpu_usage(self):
            gpu_usage = self.model_manager.memory_manager.get_gpu_memory_usage()
            self.gpu_progress_bar['value'] = gpu_usage
            self.gpu_usage_label.config(text=f"GPU: {gpu_usage}%")
            self.window.after(1000, self.update_gpu_usage)

    def toggle_model_cache(self, model, value):
            if value == 1:
                available_models[model]['cache'] = True
            else:
                available_models[model]['cache'] = False

    def process_text(self):
        selected_model = self.model_selection.get()
            
        # Parameters
        input_text = self.input_text.get("1.0", tk.END).strip()
        context_text = self.context_text.get("1.0", tk.END).strip()
        text_list = [input_text, context_text]
        task = available_models[selected_model]['task']

        # Process text
        result = self.model_manager.process_text(selected_model, text_list, task)

        # Display the result in the result text box
        self.result_text.configure(state="normal")
        self.result_text.delete("1.0", tk.END)
        self.result_text.insert(tk.END, result)
        self.result_text.configure(state="disabled")

    def run(self):
        self.update_ram_usage()
        self.update_gpu_usage()
        self.window.mainloop()

