# MarIA GUI

## PROJECT DESCRIPTION

MarIA GUI is a graphical implementation of MarIA, the first massive AI system in Spanish _trained with more than 135 billion words and 9.7 trillion operations_.

The application is responsible for downloading the chosen model from online repositories and saving it locally, processing the text and returning the result.

This way it does not depend on the limited daily uses of the web platform and response times are much faster.

## PROJECT USAGE

It provides a base to start working on to design your own AI project with this or other HuggingFace repositories.

The project is done in python, structured with the facade design pattern and using the tkinter libraries for the GUI and transformers to work with the models.

It also includes a memory management section with three modes of behavior to free RAM and GPU memory and a monitor of these.

## DOCUMENTATION

In the doc folder you will find all the documentation of the project in HTML format as well as a UML diagram with the attributes and methods of the classes and their relationship. 

Check the screenshots too to check some example uses.

## ABOUT THE MODELS

Given their size I have not included them in this repository, besides they are available in the official source that I quote in the credits. 

However for total convenience as they are needed to process the actions requested in the GUI they will be downloaded locally and for the following occasions they will be loaded locally.

## CREDITS

Thanks to the **Spanish National Library**, the **Barcelona Supercomputing Center** and the **Secretariat of State for Digitalization and Artificial Intelligence** for the realization and publication of these models in HuggingFace: 

Repository: [https://huggingface.co/PlanTL-GOB-ES]

## ABOUT

I have developed this project as a basis for a much larger personal artificial intelligence project, since up to this point the code is general-purpose and easily reusable 
I have decided to release this fragment for anyone who wants to take advantage of it or experiment with it in addition to serving as a publication for my own portfolio.

---

# MarIA GUI

## DESCRIPCIÓN DEL PROYECTO

MarIA GUI es una implementación gráfica de MarIA, el primer sistema masivo de IA en español _entrenada con más de 135 mil millones de palabras y 9,7 trillones de operaciones_.

La aplicación se encarga de descargar el modelo elegido de los repositorios online y guardarlo en local, procesar el texto y devolver el resultado.

De esta forma que no se depende de los usos limitados diarios de la plataforma web y los tiempos de respuesta son mucho más rápidos.

## USO DEL PROYECTO

Ofrece una base sobre la que empezar a trabajar para diseñar tu propio proyecto de IA con este u otros repositorios de HuggingFace.

El proyecto está realizado en python, estructurado con el patrón de diseño de fachada y utilizando las librerías de tkinter para la interfaz gráfica y transformers para trabajar con los modelos.

Además incluye un apartado de gestión de memoria con tres modos de comportamiento para liberar memoria RAM y de GPU y un monitor de estas.

## DOCUMENTACION

En la carpeta doc encontrarás toda la documentación del proyecto en formato HTML además de un diagrama UML con los atributos y métodos de las clases y su relación.

Consulta también las capturas de pantalla para ver algunos ejemplos de uso.

## SOBRE LOS MODELOS

Dado su tamaño no los he incluido en este repositorio, además están disponibles en la fuente oficial que cito en los créditos. 

Sin embargo para total comodidad a medida que sean necesarios para procesar las acciones que se soliciten en la GUI se descargaran en local y para las siguientes ocasiones se cargaran localmente.

## CREDITOS

Agradecer a la **Biblioteca Nacional Española**, al **Barcelona Supercomputing Center** y a la **Secretaría de Estado de Digitalización e Inteligencia Artificial** la realización y publicación de estos modelos en HuggingFace: 

Repositorio: [https://huggingface.co/PlanTL-GOB-ES]

## ACERCA DE

He desarrollado este proyecto como base para un proyecto personal de inteligencia artificial mucho más extenso, dado que hasta este punto el código es de uso general y fácilmente reutilizable 
he decidido liberar este fragmento para cualquiera que quiera aprovecharlo o experimentar con él además de servir de publicación para mi propio portfolio.